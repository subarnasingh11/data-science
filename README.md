# Data Science 100 days challenge

1. Week 1: Statistics
2. Week 2: Mathematics
3. Week 3: Data Wrangling
4. Week 4: Communication and Data Visualization
5. Week 5: Data Intuition
6. Week 6: Machine Learning
7. Week 7: Software Skills
8. Week 8: Linear Algebra
9. Week 9: Calculus
10. Week 10: Experiment Design
11. Week 11: Programming and Tools